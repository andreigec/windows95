export function PostApi(url, data) {
    const jdata = JSON.stringify(data);
    const ret = fetch(url, {
        method: 'POST',
        body: jdata,
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then((response) => response.json())
        .then((body) => {
            if (!body || Object.keys(body).length === 0) {
                return null;
            }

            const datanew = JSON.parse(JSON.stringify(body));
            return datanew;
        })
        .catch((error) => {
            console.error(`api error: ${error}`);
        });
    return ret;
}

export function GetApi(url) {
    const ret = fetch(url)
        .then((response) => response.json())
        .then((body) => {
            if (!body || Object.keys(body).length === 0) {
                return null;
            }

            const datanew = JSON.parse(JSON.stringify(body));
            return datanew;
        })
        .catch((error) => {
            console.error(`api error: ${error}`);
        });
    return ret;
}

export function GetApiStatus(url) {
    const ret = fetch(url)
        .then((body) => body.status)
        .catch((error) => {
            console.error(`api error: ${error}`);
        });
    return ret;
}
