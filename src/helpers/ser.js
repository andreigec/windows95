import defaults      from 'store/plugins/defaults';
import expire        from 'store/plugins/expire';
import engine        from 'store/src/store-engine';
import cookieStorage from 'store/storages/cookieStorage';
import localStorage  from 'store/storages/localStorage';

const storages = [localStorage, cookieStorage];
const plugins = [defaults, expire];
const store = engine.createStore(storages, plugins);

export function get(key) {
    const ret = store.get(key);
    if (ret) {
        return JSON.parse(ret);
    }

    return null;
}

export function set(key, value, timeoutSec = -1) {
    if (timeoutSec === -1) {
        store.set(key, JSON.stringify(value));
    } else {
        store.set(key, JSON.stringify(value), new Date().getTime() + (timeoutSec * 1000));
    }
}
