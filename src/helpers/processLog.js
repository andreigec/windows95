/* eslint spaced-comment: "off"*/
/* eslint no-console: "off"*/
/* eslint no-undef: "off"*/

function dateF() {
    const d = new Date();
    const h = d.getHours().toString().slice(-2);
    const m = d.getMinutes().toString().slice(-2);
    const s = d.getSeconds().toString().slice(-2);
    const tz = d.toString().match(/([A-Z]+[+-][0-9]+)/)[1];
    const str = `${h}:${m}:${s}(${tz})`;
    return str;
}

function nicify(...args) {
    const ret = [];
    for (let i = 0; i < args[0].length; i += 1) {
        if (args[0][i] !== null && typeof (args[0][i]) !== 'undefined' && args[0][i].toString().indexOf('Error:') !== -1) {
            ret.push(` ${args[0][i].stack}`);
        }
        if (typeof (args[0][i]) === 'string') {
            ret.push(` ${args[0][i]}`);
        } else {
            ret.push(`\r\n${JSON.stringify(args[0][i], null, 4)}`);
        }
    }

    return ret;
}

function logprocess(type, date, args) {
    let ret = `[${date}] ${type}`;

    for (let a = 0; a < args.length; a += 1) {
        ret += ` ${args[a]}`;
    }

    // console log required here
    console.log(ret.replace('  ', ' '));
}

function printStackTrace(...args) {
    const callstack = [];
    let isCallstackPopulated = false;
    try {
        i.dont.exist += 0; // doesn't exist- that's the point
    } catch (e) {
        if (e.stack) { // Firefox / chrome
            const lines = e.stack.split('\n');
            for (let i = 0, len = lines.length; i < len; i += 1) {
                callstack.push(` ${lines[i]} `);
            }

            // Remove call to logStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        } else if (window.opera && e.message) { // Opera
            const lines = e.message.split('\n');
            for (let i = 0, len = lines.length; i < len; i += 1) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_$]+\(/)) {
                    let entry = lines[i];

                    // Append next line also since it has the file info
                    if (lines[i + 1]) {
                        entry += ` at ${lines[i + 1]}`;
                        i += 1;
                    }

                    callstack.push(entry);
                }
            }

            // Remove call to logStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
    }

    if (!isCallstackPopulated) { // IE and Safari
        let currentFunction = args.callee.caller;
        while (currentFunction) {
            const fn = currentFunction.toString();
            const fname = fn.substring(fn.indexOf('function') + 8, fn.indexOf('(')) || 'anonymous';
            callstack.push(fname);
            currentFunction = currentFunction.caller;
        }
    }

    return callstack.join('\n');
}

module.exports.trace = function trace(...args) {
    if (args.logAll && args.logAll === false) {
        return;
    }

    const argsNice = nicify(args);
    args.push(printStackTrace());
    logprocess('TRACE', dateF(), argsNice);
};

module.exports.debug = function debug(...args) {
    if (args.logAll && args.logAll === false) { return; }
    logprocess('DEBUG', dateF(), nicify(args));
};

module.exports.info = function info(...args) {
    if (args.logAll && args.logAll === false) { return; }
    logprocess('INFO', dateF(), nicify(args));
};

module.exports.warn = function warn(...args) {
    logprocess('WARN', dateF(), nicify(args));
};

module.exports.error = function error(...args) {
    logprocess('ERROR', dateF(), nicify(args));
};
