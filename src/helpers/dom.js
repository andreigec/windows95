export function debounce(fn, delay) {
    let timer = null;
    return function debounceF(...args) {
        const context = this;
        clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay);
    };
}
export function isLocalhost() { return window.location.hostname === 'localhost' || window.location.hostname === '[::1]'; }
