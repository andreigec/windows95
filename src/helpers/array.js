export function findIndex(arr, fn) {
    return arr.reduce((carry, item, idx) => {
        if (fn(item, idx)) {
            return idx;
        }

        return carry;
    }, -1);
}

export function sumArray(array, propF) {
    let total = 0;
    const len = array.length;
    for (let i = 0; i < len; i += 1) {
        if (array[i]) {
            total += propF(array[i]);
        }
    }
    return total;
}

export function flattenArray(arrays) {
    return arrays.reduce(
        (a, b) => a.concat(b),
        []
    );
}

export function removeArrayItem(array, item) {
    const index = array.indexOf(item);
    if (index > -1) {
        array.splice(index, 1);
    }
}
