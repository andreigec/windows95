export function prettyJson(item) {
    try {
        if (typeof (item) === 'object') {
            const pr = JSON.stringify(item, null, 2);
            return { pretty: pr, error: false };
        }

        return { pretty: JSON.stringify(JSON.parse(item), null, 2) };
    } catch (e) {
        const error = e.toString();
        return { error };
    }
}
