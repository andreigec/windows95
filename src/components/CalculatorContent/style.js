import { inset } from '../Button/style';

export default {
    base: {
        display: 'flex',
        flexFlow: 'column',

    },
    screen: {
        width: '90%',
        height: '40px',
        margin: '0 auto',
        marginTop: '10px',
        marginBottom: '10px',
        backgroundColor: 'white',
        ...inset,
    },
    equation: {
        fontSize: '24px',
        lineHeight: '0',
        display: 'inline-block',
        paddingLeft: '5px',
    },
    numPad: {
        display: 'flex',
        flexFlow: 'row wrap',
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        flexGrow: 1,
        alignContent: 'center',
    },

    operator: {
        color: 'red',
    },
    number: {
        color: 'blue',
    },

    row: {
        display: 'flex',
        flexBasis: '100%',
        justifyContent: 'center',
    },
};
