import Button          from '../Button';
import style           from './style';
import injectSheet     from 'react-jss';
import React           from 'react';

const CalculatorContent = ({ classes, value }) => (<div className={classes.base}>
    <div className={classes.screen}>
        <p className={classes.equation}>{`${value} pwnedcoins`}</p></div>
    <div className={classes.numPad}>

        <div className={classes.row}>
            <Button className={classes.number}>7</Button>
            <Button className={classes.number}>8</Button>
            <Button className={classes.number}>9</Button>
            <Button className={classes.operator}>/</Button>
        </div>

        <div className={classes.row}>
            <Button className={classes.number}>4</Button>
            <Button className={classes.number}>5</Button>
            <Button className={classes.number}>6</Button>
            <Button className={classes.operator}>*</Button>
        </div>

        <div className={classes.row}>
            <Button className={classes.number}>1</Button>
            <Button className={classes.number}>2</Button>
            <Button className={classes.number}>3</Button>
            <Button className={classes.operator}>-</Button>
        </div>

        <div className={classes.row}>
            <Button className={classes.number}>0  </Button>
            <Button className={classes.number}>+/-</Button>
            <Button className={classes.operator}>.</Button>
            <Button className={classes.operator}>+</Button>
        </div>
    </div>
</div>);

export default injectSheet(style)(CalculatorContent);
