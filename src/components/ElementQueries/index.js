import classnames                      from 'classnames';
import React                           from 'react';

export const screenMdKey = '--screen-md';
const screenLg = 1280;

const query = {

    [`${screenMdKey}`]: {
        maxWidth: screenLg,
    },

};

/**
 * A HOC that wraps a component to make element queries possible (media query based on size of div, not viewport)
 * 1: wrap your exported component with ElementQueries, eg... export default ElementQueries(MyComponent)
 * 2: get cx from your props in your render, eg render(){const{cx}=this.props; ...
 * 3: ElementQueries will provide you with the classnames function cx. use this to wrap your classnames as per normal
 * note: if isTopLevel is set to false, will get dimensions from the nearest parent that also uses Dimensions instead of div itself
 * @param {object} Component your component
 * @param {bool} isTopLevel if true, uses own dimensions, if false, uses nearest parents' dimensions
 * @returns {function} a HOC to wrap your component with
 * */
export default (Component, isTopLevel = true) =>
    class extends React.Component {
        componentDidMount() {
            this.updateDimensions();
            window.addEventListener('resize', this.onResize, false);
        }

        componentWillUnmount() {
            window.removeEventListener('resize', this.onResize);
        }

        onResize=() => {
            if (this.rqf) {
                return;
            }
            this.rqf = window.requestAnimationFrame(() => {
                this.rqf = null;
                this.updateDimensions();
            });
        }

        getClassNamesToApply=() => {
            const queryKeys = Object.keys(query);
            const ret = queryKeys.filter((qk) => this.checkStyle({ ...query[qk], ...this.state }));
            return ret;
        }

        checkStyle=({ minWidth, maxWidth, containerWidth }) => (!minWidth || containerWidth >= minWidth) && (!maxWidth || containerWidth <= maxWidth)

        searchTreeForDataAttr=(base, attrName) => {
            let current = base;
            while (current && current.memoizedProps) {
                if (current.memoizedProps[attrName]) {
                    return current;
                }

                current = current.return;
            }
            return null;
        }

        updateDimensions=() => {
            const container = this.container._reactInternalFiber; // eslint-disable-line no-underscore-dangle
            if (!container) {
                throw new Error('Cannot find container div');
            }

            const parentNode = this.searchTreeForDataAttr(container, 'data-dimensions-top');
            let childWithSize = null;
            if (!parentNode) {
                childWithSize = container.child.stateNode;
            } else {
                childWithSize = (parentNode.child ? parentNode.child : parentNode).stateNode;
            }
            if (childWithSize) {
                this.setState({
                    containerWidth: childWithSize.clientWidth,
                    containerHeight: childWithSize.clientHeight,
                });
            }
        }

        cx=(...params) => {
            const cn = this.getClassNamesToApply();
            return classnames(...params, ...cn);
        }

        render() {
            const cw = this.state && this.state.containerWidth;
            const ch = this.state && this.state.containerHeight;
            const refs = {};
            refs['data-dimensions'] = true;
            if (isTopLevel) {
                refs['data-dimensions-top'] = true;
            }

            const cn = this.getClassNamesToApply();

            return (<Component
                ref={(r) => { this.container = r; }}
                {...refs}
                eqcx={this.cx}
                cx={this.cx}
                {...this.props}
                {...this.state}
                containerWidth={cw}
                containerHeight={ch}
                responsiveClasses={cn}
            />);
        }
    };
