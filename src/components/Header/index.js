import { coinHiveSiteKey }                  from '../../constants';
import style                                from './style';
import CoinHive                             from 'react-coin-hive';
import injectSheet                          from 'react-jss';
import React                                from 'react';

class Header extends React.PureComponent {
    render() {
        const { classes } = this.props;
        return (<div className={classes.headerBase} />);
    }
}

export default injectSheet(style)(Header);
