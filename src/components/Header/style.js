import { white }              from '../../styles/_colours';

export default {
    headerBase: {
        display: 'flex',
        flexFlow: 'row wrap',
        justifyContent: 'space-between',

        backgroundColor: white,
    },
};
