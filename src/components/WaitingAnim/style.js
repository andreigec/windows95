import { blue4 } from '../../styles/_colours';

const rect = {
    backgroundColor: blue4,
    height: '100%',
    width: '20%',
    display: 'inline-block',
    animation: 'sk-waveStretchDelay 1.2s infinite ease-in-out',
};

export default {
    body: {
        width: '100%',
        height: '100%',
        textAlign: 'center',
        fontSize: '10px',
    },
    wave: { width: '20em', height: '5em' },
    rect1: { ...rect, animationDelay: '-1.2s' },
    rect2: { ...rect, animationDelay: '-1.1s' },
    rect3: { ...rect, animationDelay: '-1.0s' },
    rect4: { ...rect, animationDelay: '-0.9s' },
    rect5: { ...rect, animationDelay: '-0.8s' },
};
