import style       from './style';
import PropTypes   from 'prop-types';
import injectSheet from 'react-jss';
import React       from 'react';

const WaitingAnim = ({ classes }) => (
    <div className={classes.wave}>
        <div className={classes.rect1} />
        <div className={classes.rect2} />
        <div className={classes.rect3} />
        <div className={classes.rect4} />
        <div className={classes.rect5} />
    </div>);

WaitingAnim.propTypes = {
    classes: PropTypes.shape({
        wave: PropTypes.string,
        rect: PropTypes.string,
        rect1: PropTypes.string,
        rect2: PropTypes.string,
        rect3: PropTypes.string,
        rect4: PropTypes.string,
        rect5: PropTypes.string,
    }).isRequired,
};

export default injectSheet(style)(WaitingAnim);
