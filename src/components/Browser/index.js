import { debounce }                    from '../../helpers/dom';
import Button                          from '../Button';
import ElementQueries                  from '../ElementQueries';
import Win95Window                     from '../Win95Window';
import style                           from './style';
import yahoo                           from './yahoo.bmp';
import sha1                            from 'js-sha1';
import injectSheet                     from 'react-jss';
import React                           from 'react';

class BrowserContent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { text: '', timeblock: false };
        this.unlock = debounce(this.unlock, 1500);
    }

    onKeyDown=(e) => {
        if (e.keyCode === 13) {
            this.submit();
        }
    }
    submit=() => {
        const { processing } = this.props;
        const { text, timeblock } = this.state;
        if (processing || timeblock || !text) {
            return;
        }
        const s1 = sha1.create();
        const hash = s1.update(text).hex();
        this.props.onSubmitSearch(hash);
        this.unlock();

        this.setState({ text: '', timeblock: true });
    }
    unlock=() => {
        this.setState({ timeblock: false });
    }

    handleChange = (event) => {
        const text = event.target.value;
        this.setState({ text });
        this.props.resetMessage();
    }

    render() {
        const { processing,
            cx, classes, statusText, statusSuccess } = this.props;
        const { text, timeblock } = this.state;
        return (<Win95Window
            className={classes.base}
            iconName="internet"
            title="passwordCracker"
            align="center"
            fileMenu={['File', 'Edit', 'View', 'Go', 'Favourites', 'Help']}
        >
            <div className={cx({ [`${classes.locked}`]: (processing || timeblock) })} />
            <input
                tabIndex={-1}
                onChange={this.handleChange}
                onKeyDown={this.onKeyDown}
                placeholder="Enter password here"
                ref={(r) => { this.inputr = r; }}
                value={text}
                className={classes.browserInput}
            />
            <Button
                tabIndex={-2}
                onClick={this.submit}
                className={cx(classes.browserButton,
                    { [`${classes.browserButtonDisabled}`]: processing }
                )}
            >Submit</Button>
            {statusText && <div className={cx(classes.browserStatus, { [`${classes.browserStatusOk}`]: statusSuccess })}>{statusText}</div>}
            <img alt="classic yahoo" className={classes.browserImage} src={yahoo} />
        </Win95Window>);
    }
}

export default injectSheet(style)(ElementQueries(BrowserContent, false));
