import {  screenMdKey } from '../ElementQueries';

export default {
    base: {
        display: 'flex',
        flexFlow: 'column',
        overflow: 'hidden',
        minHeight: '20em',
        maxWidth: '35em',
        [`&.${screenMdKey}`]: {
            maxWidth: 'inherit',
        },
    },
    browserInput: {
        position: 'absolute',
        top: '13em',
        left: '5em',
        width: '15em',
        zIndex: '2',
        height: '2em',
    },
    locked: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: '3',
        cursor: 'default',
    },

    browserButton: {
        position: 'absolute',
        zIndex: '2',
        top: '13.25em',
        left: '21em',
        width: '5em',
        height: '1.5em',
        backgroundColor: 'silver',
        cursor: 'pointer',
    },
    browserButtonDisabled: {
        cursor: 'default',
    },
    browserStatus: {
        position: 'absolute',
        top: '13em',
        left: '4.5em',
        zIndex: '2',
        color: 'red',
    },
    browserStatusOk: {
        color: 'green',
    },

    browserImage: {
        position: 'absolute',
    },
};
