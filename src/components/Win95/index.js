import { instructions }              from '../../constants';
import { icons, start, speaker }     from '../../icons';
import Briefcase                     from '../Briefcase';
import Browser                       from '../Browser';
import ElementQueries                from '../ElementQueries';
import Win95Window                   from '../Win95Window';
import style                         from './style';
import injectSheet                   from 'react-jss';
import React                         from 'react';

class Win95 extends React.PureComponent {
    render() {
        const { processing,
            storeItems,
            cx,
            classes,
            values,
            onSubmitSearch,
            statusText,
            statusSuccess,
            onBuyItem,
            resetMessage } = this.props;

        const time = new Date();
        const timestr = `${time.getHours().toString().padStart(2, '0')}:${time.getMinutes().toString().padStart(2, '0')}`;
        const startBar = (<div className={cx(classes.win95bar)}>
            <div className={classes.startButton}>
                <img alt="" src={start} width="26" height="20" />
                <span >Start</span>
            </div>
            <div className={classes.time}>
                <img className={classes.speaker} alt="" src={speaker} width="17" height="17" />
                <div className={classes.win95Timestring}>{timestr}</div></div>
        </div>);

        function generateIcon(name, text, large = false) {
            return (<li className={cx(classes.block, { [`${classes['block--large']}`]: large })}>
                <img alt="" src={icons[name]} />
                <span>{text}</span>
            </li>);
        }

        return (<div className={cx(classes.wrapper)}>
            <div className={classes.desktop}>
                <ul className={classes.icons}>
                    {generateIcon('computer', 'My computer')}
                    {generateIcon('recycle', 'Recycle bin')}
                    {generateIcon('internet', 'The Internet', true)}
                    {generateIcon('briefcase', 'My Briefcase')}
                    {generateIcon('inbox', 'Go to Inbox', true)}
                    {generateIcon('documenti', 'New Document')}
                </ul>
            </div>

            <Win95Window
                iconName="briefcase"
                title="My pwnedCoins© - Briefcase"
                align="left"
            >
                <Briefcase
                    onBuyItem={onBuyItem}
                    storeItems={storeItems}
                    values={values}
                />
            </Win95Window>

            <Browser
                resetMessage={resetMessage}
                onSubmitSearch={onSubmitSearch}
                processing={processing}
                statusText={statusText}
                statusSuccess={statusSuccess}
            />

            <Win95Window
                iconName="documenti"
                title="pwned95.txt - Notepad"
                style={{ minHeight: '40em' }}
            >
                <textarea
                    tabIndex={-3}
                    style={{ resize: 'none' }}
                    defaultValue={instructions}
                />
            </Win95Window>
            {startBar}
        </div>

        );
    }
}
export default injectSheet(style)(ElementQueries(Win95, true));
