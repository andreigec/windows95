import { icons }                                    from '../../icons';
import ElementQueries, {  screenMdKey }             from '../ElementQueries';
import style                                        from './style';
import injectSheet                                  from 'react-jss';
import React                                        from 'react';

class Win95Window extends React.PureComponent {
    getAlignClass=(responsiveClasses) => {
        const { classes, align = 'right' } = this.props;
        if (responsiveClasses.find((f) => f === screenMdKey)) {
            return classes.alignRelative;
        }

        let alignClassName = '';
        if (align === 'right') {
            alignClassName = classes.alignRight;
        } else if (align === 'center') {
            alignClassName = classes.alignCenter;
        } else if (align === 'left') {
            alignClassName = classes.alignLeft;
        }
        return alignClassName;
    }
    render() {
        const { style: importStyles,
            responsiveClasses,
            cx,
            className,
            classes, title, iconName, children, fileMenu = [] } = this.props;
        const alignClassName = this.getAlignClass(responsiveClasses);

        return (<div style={importStyles} className={cx(classes.window, alignClassName, className)}>
            <div className={classes.windowTitleBar}>
                <img className={classes.windowImage} alt="" src={icons[iconName]} />
                <span>{title}</span>
                <div className={classes.windowTitleBarButtons}>
                    <div className={classes.windowTitleBarButton} ><div className={classes.iconMinimize} /></div>
                    <div className={classes.windowTitleBarButton} ><div className={classes.iconMaximize} /></div>
                    <div className={classes.windowTitleBarButton} >✖</div>
                </div>
            </div>
            <div className={classes.fileMenu}>
                {fileMenu.map((f) => (<div className={classes.fileMenuItem} key={f}>
                    {f}
                </div>))}
            </div>
            <div className={classes.explorerContents}>
                {children}
            </div>
        </div>);
    }
}
export default injectSheet(style)(ElementQueries(Win95Window, false));
