export default {

    window: {
        backgroundColor: 'silver',
        margin: '0px',
        borderTop: '1px solid #FFF',
        borderLeft: '1px solid #FFF',
        borderRight: '1px solid #7B7F83',
        borderBottom: '1px solid #7B7F83',
        boxShadow: '1px 0 #000000, 0 1px #000000, 0 -1px #C3C7CB, -1px 0 #C3C7CB',
        padding: '2px 3px',
        position: 'absolute',
        zIndex: '4',

        minWidth: '20em',
        display: 'flex',
        flexFlow: 'column',

    },
    alignRelative: {
        position: 'relative',
    },
    alignCenter: {
        left: '25%',
        bottom: '10%',
        width: '50%',
    },
    alignRight: {
        right: '5%',
        top: '5%',
        width: '20%',
    },
    alignLeft: {
        left: '2%',
        top: '15%',
        width: '8%',
    },
    windowImage: {
        maxHeight: '120%',
        width: 'auto',
        marginRight: '1em',
    },

    windowTitleBar: {
        fontWeight: 'bold',
        background: '#0514A7',
        color: '#fff',
        lineHeight: '1.4',
        fontSize: '12px',
        letterSpacing: '1px',
        maxWidth: '100%',
        height: '1em',
        paddingLeft: '0.5em',
        paddingRight: '0.5em',
        paddingTop: '0.5em',
        paddingBottom: '0.5em',
        margin: '0',
        textAlign: 'left',
        display: 'flex',
        alignItems: 'center',
    },

    windowTitleBarButtons: {
        display: 'flex',
        marginLeft: 'auto',

        top: '5px',
        right: '3px',
        bottom: '1px',
    },

    windowTitleBarButton: {
        width: '15px',
        height: '15px',
        marginLeft: '2px',
        borderTop: '1px solid #FFF',
        borderLeft: '1px solid #FFF',
        borderRight: '1px solid #7B7F83',
        borderBottom: '1px solid #7B7F83',
        boxShadow: '1px 1px #000',
        display: 'inline-block',
        backgroundColor: '#C3C7CB',
        backgroundRepeat: 'no-repeat',
        filter: 'grayscale(100%)',
        color: 'black',
        textAlign: 'center',
    },

    iconMaximize: {
        borderBottom: '1px solid black',
        borderTop: '3px solid black',
        borderLeft: '1px solid black',
        borderRight: '1px solid black',
        height: '60%',
        width: '70%',
        display: 'block',
        margin: 'auto',
        marginTop: '1.2px',
    },

    iconMinimize: {
        borderBottom: '2px solid black',
        width: '80%',
        height: '80%',
        display: 'block',
        marigin: 'auto',
        marginLeft: '1.5px',
    },

    explorerContents: {
        backgroundColor: 'silver',
        marginTop: '0.1em',
        background: 'none repeat scroll 0% 0% #FFF',
        boxShadow: '1px 0px #000 inset, 0px 1px #000 inset, 1px 0px #FFF, 0px 1px #FFF',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: '#7B7F83 #C3C7CB #C3C7CB #7B7F83',
        width: 'calc(100% - 1px)',
        flexGrow: '1',
        display: 'inline-grid',
        overflowY: 'scroll',
        maxHeight: '40em',
    },
    fileMenu: {
        display: 'flex',
        borderTop: '1px solid #7B7F83',
        paddingTop: '0.2em',
        marginBottom: '0.2em',
    },
    fileMenuItem: {
        '&:not(:first-child)': {
            marginLeft: '1em',
        },

    },
};
