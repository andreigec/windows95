export default {
    aboutBase: {
        width: '100%',
        padding: '1rem',
    },
    aboutTitle: {
        fontSize: '3em',
        textAlign: 'center',
    },
    aboutText: {
        marginTop: '1em',
        fontSize: '1.5em',
        textAlign: 'center',
    },
};
