import pkg                               from '../../../package.json';
import { email }                         from '../../constants';
import style                             from './style';
import PropTypes                         from 'prop-types';
import injectSheet                       from 'react-jss';
import React                             from 'react';

const about = (props) => {
    const { classes } = props;
    return (<div className={classes.aboutBase}>
        <div className={classes.aboutTitle}>{pkg}</div>
        <div className={classes.aboutText}>This page was developed by <a href="https://gectek.com">gectek</a></div>
        <div className={classes.aboutText}>Contact us at <a href={email} target="_top">bollardart@gectek.com</a></div>
    </div>);
};

about.propTypes = {
    classes: PropTypes.shape({
        aboutBase: PropTypes.string.isRequired,
        aboutTitle: PropTypes.string.isRequired,
        aboutText: PropTypes.string.isRequired,
    }).isRequired,
};

export default injectSheet(style)(about);
