import Button      from '../Button';
import style       from './style';
import Decimal     from 'decimal.js-light';
import injectSheet from 'react-jss';
import React       from 'react';

class Briefcase extends React.PureComponent {
    getValue=(raw) => (raw < 10 ? (new Decimal(raw)).toFixed(2) : (new Decimal(raw)).toFixed(0))
    getRenderValue=(raw) => {
        if (raw < 2000) {
            return raw;
        }
        if (raw < 2000000) {
            return `${raw / 1000}k`;
        }
        return `${raw / 1000000}m`;
    }
    render() {
        const { onBuyItem, storeItems, classes, values: { value: raw, perSecond: perSecondRaw } } = this.props;
        const value = this.getValue(raw);
        const valueStr = this.getRenderValue(value);
        const perSecond = this.getRenderValue(this.getValue(perSecondRaw));
        return (
            <div className={classes.base}>
                <div className={classes.briefcaseTotal}>
                    {valueStr} {value === 1 ? 'coin' : 'coins'}
                </div>
                <div className={classes.briefcaseSubTotal}>
                    {perSecond} per second
                </div>
                <div className={classes.briefcaseStore}>
                    <div className={classes.storeTitle}>
                Store
                    </div>
                    <div className={classes.screen}>
                        {storeItems.map((item) => (
                            <Button inverted={value < item.value} key={item.label} onClick={() => onBuyItem(item)}>
                                <div className={classes.storeItem}>
                                    <div className={classes.storeRow}>{item.label}</div>
                                    <div className={classes.storeRow}>{this.getRenderValue(item.value)} (+{this.getRenderValue(item.perSecond)})</div>
                                </div>

                            </Button>))}
                    </div>

                </div>

            </div>);
    }
}

export default injectSheet(style)(Briefcase);
