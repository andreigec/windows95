import { inset } from '../Button/style';

export default {
    base: {
        display: 'flex',
        flexFlow: 'column',

    },
    screen: {
        ...inset,
        cursor: 'default',
        width: '90%',
        margin: '0 auto',
        marginTop: '10px',
        marginBottom: '10px',
        backgroundColor: 'white',
        display: 'flex',
        flexFlow: 'row wrap',
    },
    briefcaseTotal: {
        textAlign: 'center',
        fontSize: '2em',
        color: 'white',
        backgroundColor: '#333',
    },
    briefcaseSubTotal: {
        textAlign: 'center',
        fontSize: '1.5em',
        color: 'white',
        backgroundColor: '#333',
    },
    briefcaseStore: {
        marginTop: '1em',
    },
    storeTitle: {
        fontSize: '2em',
        textAlign: 'center',
    },
    storeItem: {
        display: 'flex',
        flexFlow: 'row wrap',
    },
    storeRow: {
        flexBasis: '100%',
    },

};
