import { leaked, notleaked } from '../../constants';
import { getPwned }          from '../../ducks/pwned';
import { sumArray }          from '../../helpers/array';
import { get, set }          from '../../helpers/ser';
import Win95                 from '../Win95';
import storeItems            from './storeItems';
import style                 from './style';
import injectSheet           from 'react-jss';
import { connect }           from 'react-redux';
import React                 from 'react';

class Home extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            statusText: '',
            statusSuccess: true,
            storeItems: this.getStoreItems(),
            accumulatedValue: this.getAccumulatedValue(),
        };

        this.timer = setInterval(this.onTimer, 100);
    }

    componentWillReceiveProps(nextProps) {
        const { pwned: { lastSuccess, processing } } = nextProps;

        if (processing) {
            return;
        }

        if (lastSuccess) {
            this.pushMessage(leaked, false);
        } else {
            this.pushMessage(notleaked, true);
        }
    }

    onSubmitSearch=(pw) => {
        const { pwned: { pwned, notpwned } } = this.props;
        if (pwned.find((p) => p === pw)) {
            this.pushMessage('already added', false);
            return;
        }

        if (notpwned.find((p) => p === pw)) {
            this.pushMessage('already added', false);
            return;
        }

        this.props.dispatch(getPwned(pw));
        this.setState({ statusText: '', statusSuccess: '' });
    }

    onBuyItem=(item) => {
        const newv = this.getValue() - item.value;
        if (newv < 0) {
            this.pushMessage('you cant afford this', false);
            return;
        }

        const newStoreItems = JSON.parse(JSON.stringify(this.state.storeItems)).filter((s) => s);
        newStoreItems.push(item);
        set('storeItems', newStoreItems.map((s) => s.key));
        this.pushMessage(`bought! you have ${newv} coins left`);
        this.setState({ ...this.state, storeItems: newStoreItems });
    }

    onTimer=() => {
        try {
            const ps = this.getPerSecond() / 10;

            let acc = get('accumulatedValue')||0;
            acc += ps;

            set('accumulatedValue', acc);
            this.setState({ accumulatedValue: acc });
        } catch (e) {
            clearTimeout(this.timer);
        }
    }

    getPerSecond=() => sumArray(this.state.storeItems, (s) => s.perSecond)

    getValue=() => (this.props.pwned.pwned.length + this.state.accumulatedValue) - sumArray(this.state.storeItems, (s) => s.value)

    getStoreItems=() => {
        const ser = get('storeItems');
        if (ser) {
            return ser.map((s) => storeItems.find((k) => k.key === s));
        }

        set('storeItems', []);
        return [];
    }

    getAccumulatedValue=() => {
        const acc = get('accumulatedValue');
        if (acc) {
            return acc;
        }

        set('accumulatedValue', 0);
        return 0;
    }

    pushMessage=(statusText, statusSuccess) => {
        this.setState({ statusText, statusSuccess });
    }

    render() {
        const { statusText, statusSuccess } = this.state;
        const values = { storeItems: this.state.storeItems, value: this.getValue(), perSecond: this.getPerSecond() };

        return (
            <div className={this.props.classes.homeBase}>
                <Win95
                    onBuyItem={this.onBuyItem}
                    storeItems={storeItems}
                    resetMessage={this.pushMessage}
                    processing={this.props.pwned.processing}
                    statusText={statusText}
                    statusSuccess={statusSuccess}
                    onSubmitSearch={this.onSubmitSearch}
                    values={values}
                />

            </div>
        );
    }
}

const connected = injectSheet(style)(connect((state) => ({
    pwned: state.pwned,
    request: state.request }))(Home));
export { connected as Home };
