export default [
    { label: 'run bitcoin miner', value: 10, perSecond: 0.1, key: 'up1' },
    { label: 'buy server', value: 115, perSecond: 1, key: 'up2' },
    { label: 'buy server rack', value: 1200, perSecond: 8, key: 'up3' },
    { label: 'buy super computer', value: 14000, perSecond: 47, key: 'up4' },
    { label: 'buy quantum server', value: 150000, perSecond: 260, key: 'up5' },
    { label: 'buy data center', value: 1610000, perSecond: 7800, key: 'up7' },
    { label: 'buy dedicated national fiber', value: 380000000, perSecond: 44000, key: 'up8' },
    { label: 'buy dedicated international fiber', value: 5800000000, perSecond: 260000, key: 'up9' },
    { label: 'buy ISS', value: 88800000000, perSecond: 1600000, key: 'up10' },
];
