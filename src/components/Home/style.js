export default {

    homeBase: {
        width: '100%',
        height: '100%',
        flexGrow: 1,
        display: 'flex',
        flexFlow: 'column',
    },
};
