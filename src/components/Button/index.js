import style           from './style';
import cx              from 'classnames';
import injectSheet     from 'react-jss';
import React           from 'react';

const Button = ({ inverted, tabIndex, classes, className, children, onClick }) => (
    <button
        tabIndex={tabIndex}
        onClick={onClick}
        className={cx(classes.button, className, {
            [`${classes.buttonInverted}`]: inverted,
        })}
    >{children}</button>);

export default injectSheet(style)(Button);
