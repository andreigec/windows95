export const outset = {
    borderTop: '1px solid #fff',
    borderLeft: '1px solid #fff',
    borderRight: '1px solid gray',
    borderBottom: '1px solid gray',
    boxShadow: 'inset 1px 1px #dfdfdf, 1px 0 #000, 0 1px #000, 1px 1px #000',
};
export const inset = {
    borderTop: '1px solid gray',
    borderLeft: '1px solid gray',
    borderRight: '1px solid #dfdfdf',
    borderBottom: '1px solid #dfdfdf',
    boxShadow: '1px 0 #fff, 0 1px #fff, 1px 1px #fff',
};

export default{
    button: {
        color: '#000',
        borderTop: '1px solid #fff',
        borderLeft: '1px solid #fff',
        borderRight: '1px solid gray',
        borderBottom: '1px solid gray',
        minWidth: '2.5em',
        minHeight: '2em',
        margin: '0.2em',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        fontWeight: 'bold',
        fontFamily: 'serif',
        ...outset,
        cursor: 'pointer',
    },
    buttonInverted: {
        ...inset,
        cursor: 'default',
    },
};
