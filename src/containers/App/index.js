import About           from '../../components/About';
import Header          from '../../components/Header';
import { Home }        from '../../components/Home';
import NotFound        from '../NotFound';
import style           from './style';
import PropTypes       from 'prop-types';
import injectSheet     from 'react-jss';
import { connect }     from 'react-redux';
import { Route }       from 'react-router-dom';
import { Switch }      from 'react-router';
import React           from 'react';

const App = (props) => {
    const { classes } = props;

    return (<div className={classes.appBase}>
        <Header />
        <div className={classes.appContent}>
            <Switch location={window.location}>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/not-found" component={NotFound} />
            </Switch>
        </div>
    </div>);
};

App.propTypes = {
    classes: PropTypes.shape({
        appBase: PropTypes.string.isRequired,
        appContent: PropTypes.string.isRequired,
    }).isRequired,
};

const connected = injectSheet(style)(connect((state) => ({
    data: state.data,
    request: state.request }))(App));
export { connected as App };
