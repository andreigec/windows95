import { grey6, grey8 }           from '../../styles/_colours';

export default {
    appBase: {
        display: 'flex',
        flexFlow: 'column',
        minHeight: '100vh',
        backgroundColor: grey8,
        overflow: 'hidden',
        fontSize: '16px',
    },
    appContent: {
        display: 'flex',
        flexGrow: '1',
        flexFlow: 'column',
        borderTop: `solid 1px ${grey6}`,
        justifyContent: 'center',
        alignItems: 'center',
    },
};
