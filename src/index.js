import './index.less';
import ScrollToTop                   from './components/ScrollToTop';
import { App }                       from './containers/App';
import { configureStore }            from './ducks/_initStore';
import { initApp }                   from './ducks/request';
import registerServiceWorker         from './registerServiceWorker';
import Decimal                       from 'decimal.js-light';
import createHistory                 from 'history/createBrowserHistory';
import ReactDOM                      from 'react-dom';
import { Provider }                  from 'react-redux';
import { ConnectedRouter as Router } from 'react-router-redux';
import React                         from 'react';

Decimal.set({
    precision: 20,
    rounding: Decimal.ROUND_HALF_UP,
    toExpNeg: -7,
    toExpPos: 21,
});

const { store, history } = configureStore({}, createHistory());
store.dispatch(initApp(store, history));

const reg = (<Provider store={store}>
    <Router history={history}>
        <ScrollToTop>
            <App />
        </ScrollToTop>
    </Router>
</Provider>);

const elm = document.getElementsByClassName('react-root')[0];
ReactDOM.render(reg, elm);
registerServiceWorker();
