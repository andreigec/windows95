
export const fontLight = {
    fontFamily: '"Source Sans Pro", sans-serif',
    fontWeight: '300',
};

export const fontReg = {
    fontFamily: '"Source Sans Pro", sans-serif',
    fontWeight: '400',
};

export const fontBold = {
    fontFamily: '"Source Sans Pro", sans-serif',
    fontWeight: '600',
};
