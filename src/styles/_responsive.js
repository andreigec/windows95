export const tabletSize = '600px';
export const tablet = `@media (max-width: ${tabletSize})`;

export const desktopSize = '601px';
export const desktop = `@media (min-width: ${desktopSize})`;

export const headerLogoWidth = 21;
export const headerLogoHeight = 14;
export const headerLogoHeightMini = 4;
