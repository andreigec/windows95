import { rootReducer }            from './_reducers';
import { rootSaga }               from './_sagas';
import { routerMiddleware }       from 'react-router-redux';
import createSagaMiddleware       from 'redux-saga';
import {
    applyMiddleware,
    compose,
    createStore,
} from 'redux';

function configureStore(initialState, history) {
    const sagaMiddleware = createSagaMiddleware();
    const historyMiddleware = routerMiddleware(history);
    const finalCreateStore = compose(
        applyMiddleware(sagaMiddleware),
        applyMiddleware(historyMiddleware),
        typeof window === 'object' && typeof window.devToolsExtension !==
        'undefined' ? window.devToolsExtension() : (f) => f,
    )(createStore);

    const store = finalCreateStore(rootReducer, initialState);

    sagaMiddleware.run(rootSaga);
    return { store, history };
}

export { configureStore };
