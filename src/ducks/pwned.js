import { GetApiStatus }         from '../helpers/api';
import { get, set }             from '../helpers/ser';
import { APP_INIT }             from './request';
import { put, take, fork }      from 'redux-saga/effects';

const GET_PWNED = 'GET_PWNED';
const RETRIEVED_PWNED = 'RETRIEVED_PWNED';

export function getPwned(pw) {
    return {
        type: GET_PWNED,
        pw,
    };
}

export function retrievedPwned(pw, ispwned) {
    return {
        type: RETRIEVED_PWNED,
        pw,
        ispwned,
    };
}

const initialState = {
    pwned: [],
    notpwned: [],
    lastDate: null,
    processing: false,
    lastSuccess: false,
};

export function reducer(state = initialState, action) {
    switch (action.type) {
        case APP_INIT: {
            const oldstate = get('pwned');
            return { ...initialState, ...oldstate };
        }
        case GET_PWNED: {
            return { ...state, processing: true };
        }
        case RETRIEVED_PWNED: {
            const { pw, ispwned } = action;
            const newPwned = JSON.parse(JSON.stringify(state.pwned));
            const newNotPwned = JSON.parse(JSON.stringify(state.notpwned));
            const lastDate = new Date();
            let lastSuccess = false;

            if (ispwned) {
                lastSuccess = true;
                if (!newPwned.find((f) => f === pw)) {
                    newPwned.push(pw);
                }
            } else if (!newNotPwned.find((f) => f === pw)) {
                newNotPwned.push(pw);
            }

            const newstate = {
                ...state,
                lastDate,
                pwned: newPwned,
                notpwned: newNotPwned,
                processing: false,
                lastSuccess,
            };
            set('pwned', newstate);
            return newstate;
        }

        default:
            return state;
    }
}

function* pwnedCacheTrigger(store, ty) {
    const triggerEnabled = true;
    while (triggerEnabled) {
        const { pw } = yield take(ty);

        const { pwned, notpwned } = store.getState().pwned;
        if (pwned.find((f) => f === pw)) {
            yield put(retrievedPwned(pw, true));
            return;
        }

        if (notpwned.find((f) => f === pw)) {
            yield put(retrievedPwned(pw, false));
            return;
        }

        const path = 'https://haveibeenpwned.com/api/v2/pwnedpassword/';
        let ob = '';

        ob = yield GetApiStatus(`${path}${pw}?originalPasswordIsAHash=true`);
        yield put(retrievedPwned(pw, ob === 200));
    }
}

export function* exposeSagas(store) {
    yield fork(pwnedCacheTrigger, store, GET_PWNED);
}
