import { reducer as pwned }            from './pwned';
import { reducer as request }          from './request';
import { routerReducer }               from 'react-router-redux';
import { combineReducers }             from 'redux';

const rootReducer = combineReducers({
    request,
    pwned,
    routing: routerReducer,
});

export { rootReducer };
