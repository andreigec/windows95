export const APP_INIT = 'APP_INIT';
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';

export function initApp(store) {
    return {
        type: APP_INIT,
        store,
    };
}

const initialState = {
    userAgent: '',
    lang: 'en',
    country: '',
    myhistory: [],
    history: {},
    hostname: '',
    pathName: '',
    search: '',
};

export function reducer(state = initialState, action) {
    console.log('action=', action.type);
    switch (action.type) {
        case APP_INIT: {
            return state;
        }

        case LOCATION_CHANGE: {
            const { pathname: newPathName, search: newSearch } = window.location;

            const newhistory = JSON.parse(JSON.stringify(state.myhistory));
            newhistory.unshift(newPathName + newSearch);

            return {
                ...state,
                myhistory: newhistory,
                pathName: newPathName,
            };
        }

        default:
            return state;
    }
}
