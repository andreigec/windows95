import { exposeSagas as pwned }               from './pwned';
import { APP_INIT }                           from './request';
import { take }                               from 'redux-saga/effects';

function* rootSaga() {
    const {
        store,
    } = yield take(APP_INIT);
    yield* pwned(store);
}
export { rootSaga };
