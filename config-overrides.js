const path = require('path');
const { getLoader } = require('react-app-rewired');

const rewireLess = require('react-app-rewire-less');

function rewriteSVG(configIn, env) {
    const config = configIn;

    const extension = /\.svg/;

    const fileLoader = getLoader(
        config.module.rules, (rule) => rule.loader && typeof rule.loader === 'string' && rule.loader.endsWith(`file-loader${path.sep}index.js`)
    );
    fileLoader.exclude.push(extension);

    const rule = {
        test: extension,
        exclude: /node_modules/,
        loader: 'svg-react-loader',
        query: {
            classIdPrefix: '[name]-[hash:8]__',
        },
    };
    config.module.rules.push(rule);

    return config;
}

/* config-overrides.js */
module.exports = function override(configIn, env) {
    let config = configIn;
    config = rewireLess(config, env);
    config = rewriteSVG(config, env);
    return config;
};
